#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAXVAL 6

/* TODO
 * implement game
 */

struct tile {
	int top;
	int bottom;
};
struct stack {
	struct tile data[(MAXVAL+1)*(MAXVAL+2)/2];
	int head;
};
void push(struct tile tile, struct stack *stack) {
	stack->head++;
	stack->data[stack->head] = tile;
}
struct tile pop(struct stack *stack) {
	struct tile tile = stack->data[stack->head];
	stack->head--;
	return tile;
}
void focus(int position, struct stack *stack) {
	if(stack->data[position].top != stack->data[stack->head].top){
		stack->data[position].top = stack->data[stack->head].top ^ stack->data[position].top;
		stack->data[stack->head].top = stack->data[stack->head].top ^ stack->data[position].top;
		stack->data[position].top = stack->data[stack->head].top ^ stack->data[position].top;
	}
	if(stack->data[position].bottom != stack->data[stack->head].bottom){
		stack->data[position].bottom = stack->data[stack->head].bottom ^ stack->data[position].bottom;
		stack->data[stack->head].bottom = stack->data[stack->head].bottom ^ stack->data[position].bottom;
		stack->data[position].bottom = stack->data[stack->head].bottom ^ stack->data[position].bottom;
	}
}
void flip(struct tile *tile) {
	if(tile->top != tile->bottom){
		tile->top = tile->top ^ tile->bottom;
		tile->bottom = tile->top ^ tile->bottom;
		tile->top = tile->top ^ tile->bottom;
	}
}

int main() {
	srand(time(NULL));
	struct stack tilebox = {.head=-1};
	for(int i=0; i<=MAXVAL; i++){
		for(int j=i; j<=MAXVAL; j++){
			struct tile temp = {i,j};
			push(temp, &tilebox);
			// printf("[%d|%d] ", tilebox.data[tilebox.head].top, tilebox.data[tilebox.head].bottom);
		}
	}
	struct stack boneyard = {.head=-1};

	while(tilebox.head > -1){
		focus(rand()%(tilebox.head+1), &tilebox);
		push(pop(&tilebox), &boneyard);
		printf("[%d|%d]", boneyard.data[boneyard.head].top, boneyard.data[boneyard.head].bottom);
	}
	return 0;
}
